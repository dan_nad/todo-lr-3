FROM python:3.10-slim
MAINTAINER Kirill 2020-4-04-Bor <kirillmolinillo@gmail.com>
ENV REFRESHED_AT 2020-12-06
RUN set -e

COPY ./requirements.txt /code/requirements.txt

ARG PROXY
RUN if [ -z "$PROXY" ] ; then \
        echo no PROXY; \
        pip install --no-cache-dir --upgrade -r /code/requirements.txt; \
    else \
        echo "$PROXY"; \
        echo  "Acquire::http::Proxy \"${PROXY}\";" >> /etc/apt/apt.conf; \
        pip install --no-cache-dir --upgrade --proxy "$PROXY" -r /code/requirements.txt; \
    fi

COPY ./app /code/app
WORKDIR /code/app
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80", "--reload"]
