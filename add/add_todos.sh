#!/bin/bash
ip=localhost
port=80
if [ -n "$1" ]; then
  echo "Generate $1 todos into http://$ip:$port/add?skip=0"
else
  echo "Enter quantity todo for add!"
  echo "Format: ./add_todos.sh [NUM]"
  exit
fi

for ((i = 1; i <= $1; i++)); do
  if [[ $i%3 -eq 0 ]]; then
    tag="study"
  elif [[ $i%3 -eq 1 ]]; then
    tag="personal"
  else
    tag="plans"
  fi
  curl --location --request POST "http://$ip:$port/add?skip=0"\
    --form "title=${i}_$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c10)" \
    --form "tag=${tag}"
  echo
done
