
### Запустить сервер:
*Выполните, чтобы запустить веб-приложение таск-менеджер "ToDo" по адресу http://0.0.0.0:8000/ :*

Скачайте репозиторий и перейдите в его папку
```bash
git clone git@gitlab.com:dan_nad/todo-lr3.git
cd todo-lr3
```
Запустите docker контейнеры
```bash
docker compose up --build
```
Снести docker контейнеры
```bash
sudo docker compose down
```

