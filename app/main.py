"""Main of todo app
"""

import random


from datetime import datetime

from random import randint

from fastapi import *
from fastapi.encoders import *
from fastapi.security import *
from fastapi.templating import *
from fastapi.staticfiles import *
from fastapi.responses import *
from fastapi.openapi.models import *
from fastapi.security.base import *
from fastapi.security.utils import *

import jwt
import pandas as pd
import pathlib
import requests
import squarify  # pylint: disable=import-error
import matplotlib.pyplot as plt
import models
import pandas as pd
import random
import squarify  # pylint: disable=import-error

from fastapi.openapi.models import OAuthFlows as OAuthFlowsModel
from fastapi.security import OAuth2PasswordRequestForm, OAuth2
from fastapi.security.utils import get_authorization_scheme_param
from datetime import datetime, timedelta
from database import init_db, get_db, Session
from loguru import logger  # pylint: disable=import-error
from passlib.context import CryptContext
from pydantic import BaseModel
from starlette.status import HTTP_403_FORBIDDEN
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func, create_engine, MetaData
from typing import Optional, Union

init_db()

# to get a string like this run:
# openssl rand -hex 32
SECRET_KEY = "c51a4223d7a56a7923b3c6e44c0eed68ad88eb60646edd6b67a655f4a6fded4c"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


# fake_users_db = {
#     "johndoe": {
#         "username": "johndoe",
#         "full_name": "John Doe",
#         "email": "johndoe@example.com",
#         "hashed_password": "$2b$12$EixZaYVK1fsbw1ZfbX3OXePaWxn96p36WQoeG6Lruj3vjPGga31lW",
#         "disabled": False,
#     }
# }

def loadSession():
    """new session"""
    # pylint: disable=invalid-name
    ENGINE = create_engine("postgresql://postgres:postgres@172.17.0.1:5432", echo=True)
    SESSIONLOCAL = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)
    # pylint: enable=invalid-name

    metadata = MetaData(ENGINE)
    session = SESSIONLOCAL()
    return session


class OAuth2PasswordBearerCookie(OAuth2):
    def __init__(
            self,
            tokenUrl: str,
            scheme_name: str = None,
            scopes: dict = None,
            auto_error: bool = True,
    ):
        if not scopes:
            scopes = {}
        flows = OAuthFlowsModel(password={"tokenUrl": tokenUrl, "scopes": scopes})
        super().__init__(flows=flows, scheme_name=scheme_name, auto_error=auto_error)

    async def __call__(self, request: Request) -> Optional[str]:
        header_authorization: str = request.headers.get("Authorization")
        cookie_authorization: str = request.cookies.get("Authorization")
        logger.info(request.cookies.values())
        header_scheme, header_param = get_authorization_scheme_param(
            header_authorization
        )
        cookie_scheme, cookie_param = get_authorization_scheme_param(
            cookie_authorization
        )

        if header_scheme.lower() == "bearer":
            authorization = True
            scheme = header_scheme
            param = header_param

        elif cookie_scheme.lower() == "bearer":
            authorization = True
            scheme = cookie_scheme
            param = cookie_param

        else:
            authorization = False
        logger.info(authorization)
        # logger.info(scheme.lower())
        if not authorization or scheme.lower() != "bearer":
            if self.auto_error:
                raise HTTPException(
                    status_code=HTTP_403_FORBIDDEN, detail="Not authenticated"
                )
            else:
                return None
        return param


templates = Jinja2Templates(directory="templates")

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearerCookie(tokenUrl="/token")

app = FastAPI(docs_url=None, redoc_url=None, openapi_url=None)

app.mount("/static", StaticFiles(directory="static"), name="static")


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def get_user(db, username: str):
    user = db.query(models.UserInDB).filter(models.UserInDB.username == username).first()
    db.close()
    try:
        if user.username == username:
            return user
    except:
        print("erxdctfvghbjnkml,klmnihbugytvrccvybuhnjm")
        return None


def authenticate_user(db, username: str, password: str):
    user = get_user(db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(token: str = Depends(oauth2_scheme)):
    logger.info(" get_current_user !")
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials"
    )
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    username: str = payload.get("sub")
    print(username)
    if username is None:
        logger.info(" get_current_user None 1 !")
        return None
    token_data = models.TokenData(username=username)
    session = loadSession()
    user = get_user(session, username=token_data.username)
    if user is None:
        logger.info(" get_current_user None 2 !")
        return None
    return user


async def get_current_active_user(current_user: models.User = Depends(get_current_user)):
    logger.info("_________________get_current_active_user")
    if not current_user:
        logger.info(" get_current_active_user 1 !")
        return None
    if current_user.disabled:
        logger.info(" get_current_active_user 2 !")
        return None
    logger.info(" get_current_active_user 3 !")
    return current_user


@app.get("/cabinet")
async def read_users_me(request: Request,
                        database: Session = Depends(get_db)):
    """returns current user"""

    usr = database.query(models.UserInDB).filter(models.UserInDB.disabled == False).first()
    if usr is not None:
        return templates.TemplateResponse("profile.html", {"current_user": usr, "request": request, "authorized": True})
    return templates.TemplateResponse("profile.html", {"current_user": None, "request": request, "authorized": False})


@app.post("/token", response_model=models.Token)
async def route_login_access_token(form_data: OAuth2PasswordRequestForm = Depends(),
                                   database: Session = Depends(get_db)):
    user = authenticate_user(database, form_data.username, form_data.password)
    if not user:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    access_token_expires = datetime.timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@app.get("/sign_up")
async def sign_up(request: Request, sign_in: bool = False):
    return templates.TemplateResponse("login.html", {"request": request, "sign_in": sign_in})


@app.post("/sign_up")
async def sign_up_post(username: str = Form(default="user"), password: str = Form(default="user"),
                       database: Session = Depends(get_db)):
    if database.query(models.UserInDB).filter(models.UserInDB.username == username).first():
        return RedirectResponse(url=f'{app.url_path_for("home")}', status_code=status.HTTP_303_SEE_OTHER)
    data = models.UserInDB(username=username)
    data.hashed_password = get_password_hash(password)
    database.add(data)
    database.commit()
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/login")
async def login(request: Request, sign_in: bool = True):
    return templates.TemplateResponse("login.html", {"request": request, "sign_in": sign_in})


@app.post("/login")
async def login(request: Request, username: str = Form(default="user"), password: str = Form(default="user"),
                database: Session = Depends(get_db)):
    user = get_user(database, username)
    if user is not None:
        user = authenticate_user(database, username, password)
        logger.info(user)
        if not user:
            raise HTTPException(status_code=400, detail="Incorrect email or password")
        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(
            data={"sub": username}, expires_delta=access_token_expires
        )
        token = jsonable_encoder(access_token)
        response = RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
        response.set_cookie(
            "Authorization",
            value=f"Bearer {token}",
        )
        user.disabled = False
        database.add(user)
        database.commit()
        return response
    else:
        return templates.TemplateResponse("login.html", {"request": request, "sign_in": True})


@app.get("/logout")
async def route_logout(database: Session = Depends(get_db),
                       current_user: models.UserInDB = Depends(get_current_active_user)):
    response = RedirectResponse(url="/")
    response.delete_cookie("Authorization", domain="172.17.0.1")
    user = get_user(database, current_user.username)
    if user is not None:
        user.disabled = True
        database.add(user)
        database.commit()
    return response


def get_home_url(skip: int = 0, textarea: str | None = None, limit: int = 10):
    """
    Return url of homepage with qwery parameters.

    Parameters
    ----------
        skip : int
            Element of pagination.
        textarea : str | None
            Field for save invalid text in textarea (html).
    """
    if skip == 0 and textarea is None and limit == 10:
        return f'{app.url_path_for("home")}list'
    ans = f'{app.url_path_for("home")}list?'
    list_add = []
    if skip != 0:
        list_add.append(f'skip={skip}')
    if limit != 10:
        list_add.append(f'limit={limit}')
    if textarea is not None:
        list_add.append(f'textarea={textarea}')
    ans += "&".join(list_add)
    logger.info(f"URL for Redirect : {ans}")
    return ans


def str_to_int(string: str, default: int = 0):
    """
        Convert str to int for pagination.
        If number less than 0, using default value.

        Parameters
        ----------
            string : str
                String to casting in integer.
            default : int
                Default value.
    """
    if string.isdigit():  # Не распознает отрицательные числа и возвращает False.
        return int(string)
    return default


@app.get("/homepage")
async def todo_homepage_get(request: Request, database: Session = Depends(get_db), ):
    """Get todo homepage
    """
    active_user = database.query(models.UserInDB).filter(models.UserInDB.disabled == False).first()
    authorized = False
    if active_user:
        authorized = True
    logger.info("In homepage")
    return templates.TemplateResponse("homepage.html", {"request": request,
                                                        "authorized": authorized})


@app.get("/")
async def home(request: Request, database: Session = Depends(get_db)):
    """Main page
    """
    logger.info("In my home")
    active_user = database.query(models.UserInDB).filter(models.UserInDB.disabled == False).first()
    authorized = False
    if active_user:
        authorized = True

    return templates.TemplateResponse("base.html", {"request": request, "authorized": authorized})


@app.get("/list")
async def list_page(request: Request,
                    skip: str = "0",
                    limit: str = "10",
                    textarea: str | None = None,
                    database: Session = Depends(get_db)):
    """page with todo list
    """
    active_user = database.query(models.UserInDB).filter(models.UserInDB.disabled == False).first()
    authorized = False
    if active_user:
        authorized = True
    logger.info("In /list")
    logger.warning(f"{limit=}!!!")
    skip = str_to_int(skip, 0)
    limit = str_to_int(limit, 10)
    todos = database.query(models.Todo).order_by(models.Todo.id.desc())
    count = todos.count()
    if count > limit and skip > count - limit:
        logger.warning(f"skip > count - limit, {skip=}, {limit=}, {count=}")
        skip = count - limit
        todos = todos.offset(skip).limit(limit)
    todos = todos.offset(skip).limit(limit)
    return templates.TemplateResponse("index.html",
                                      {"request": request,
                                       "todos": todos,
                                       "skip": skip,
                                       "limit": limit,
                                       "count": count,
                                       "textarea": textarea,
                                       "authorized": authorized
                                       })


@app.post("/list")
async def list_page(request: Request,
                    skip: str = "0",
                    limit: str = "10",
                    new_limit: str = Form(),
                    textarea: str | None = None,
                    database: Session = Depends(get_db)):
    """page with todo list
    """
    logger.info("In /list POST")
    skip = str_to_int(skip, 0)
    limit = str_to_int(limit if not new_limit else new_limit, 10)
    logger.warning(f"{limit=}, {new_limit=}, {limit if not new_limit else new_limit}")
    todos = database.query(models.Todo).order_by(models.Todo.id.desc())
    count = todos.count()
    if count > limit and skip > count - limit:
        logger.warning(f"skip > count - limit, {skip=}, {limit=}, {count=}")
        skip = count - limit
        todos = todos.offset(skip).limit(limit)
    todos = todos.offset(skip).limit(limit)
    logger.warning(f"{limit=}!!!")
    return templates.TemplateResponse("index.html",
                                      {"request": request,
                                       "todos": todos,
                                       "skip": skip,
                                       "limit": limit,
                                       "count": count,
                                       "textarea": textarea
                                       })


@app.get("/import_export")
async def todo_import_export(request: Request, database: Session = Depends(get_db)):
    """Get page import_export
    """
    active_user = database.query(models.UserInDB).filter(models.UserInDB.disabled == False).first()
    authorized = False
    if active_user:
        authorized = True
    logger.info("import_export page")
    return templates.TemplateResponse("import_export.html", {"request": request,
                                                             "authorized": authorized})


@app.post("/add")
async def todo_add(skip: int = 0,
                   limit: int = 10,
                   title: str = Form(None),
                   tag: str = Form("Default"),
                   database: Session = Depends(get_db),
                   file: UploadFile = File(None),
                   current_user: models.User = Depends(get_current_active_user)):
    """Add new todo
    """
    logger.info("add page")
    if title is None:
        logger.warning("Creating empty todo interrupted")
        title = ""
        return RedirectResponse(url=get_home_url(skip, textarea=title, limit=limit),
                                status_code=status.HTTP_303_SEE_OTHER)
    if len(title) > 500:
        logger.warning("Creating too long (<500 chars) todo interrupted")
        return RedirectResponse(url=get_home_url(skip, textarea=title, limit=limit),
                                status_code=status.HTTP_303_SEE_OTHER
                                )
    if not current_user:
        todo = models.Todo(title=title, tag_choose=tag, fullname="guest")
        logger.info(f"Creating todo: {todo}")
        todo.current_datetime = datetime.now().date()
        database.add(todo)
        database.commit()
        return RedirectResponse(url=get_home_url(skip, limit=limit), status_code=status.HTTP_303_SEE_OTHER) 
    else:
        if file.filename != "":
            content = file.file.read()
            path_to_image = f"todos_images/{file.filename.replace('','_')}"
            with open(path_to_image, "wb")as file_:
                file_.write(content)
    todo = models.Todo(title=title, tag_choose=tag, fullname=current_user.username)
    logger.info(f"Creating todo: {todo}")
    todo.current_datetime = datetime.now().date()
    database.add(todo)
    database.commit()
    return RedirectResponse(url=get_home_url(skip, limit=limit), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/edit/{todo_id}")
async def todo_get(request: Request, todo_id: int, skip: int = 0, limit: int = 10,
                   database: Session = Depends(get_db)):
    """Get todo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if todo is None:
        logger.warning("Editing not exist todo interrupted")
        return RedirectResponse(url=get_home_url(skip, limit=limit), status_code=status.HTTP_303_SEE_OTHER)
    logger.info(f"Getting todo: {todo}, {todo.tag_choose}")
    return templates.TemplateResponse("edit.html", {"request": request,
                                                    "todo": todo,
                                                    "Tag": models.Tag,
                                                    "skip": skip,
                                                    "limit": limit
                                                    })


@app.post("/edit/{todo_id}")
# pylint: disable=too-many-arguments
async def todo_edit(
        request: Request,
        todo_id: int,
        skip: int = 0,
        limit: int = 10,
        title: str = Form(None),
        tag_choose: str = Form(None),
        details: str = Form(""),
        completed: bool = Form(False),
        database: Session = Depends(get_db)):
    """Edit todo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if todo is None:
        logger.warning("Editing not exist todo interrupted")
        return RedirectResponse(url=get_home_url(skip, limit=limit), status_code=status.HTTP_303_SEE_OTHER)
    if title is None:
        logger.warning("Creating empty todo interrupted")
        return templates.TemplateResponse("edit.html", {"request": request,
                                                        "todo": todo,
                                                        "skip": skip,
                                                        "limit": limit
                                                        })
    if len(title) > 500:
        todo.title = title  # Do not commit into database
        textarea = True
        logger.warning("Creating too long (<500 chars) todo interrupted")
        return templates.TemplateResponse("edit.html", {"request": request,
                                                        "todo": todo,
                                                        "skip": skip,
                                                        "limit": limit,
                                                        "textarea": textarea,
                                                        "Tag": models.Tag
                                                        })
    logger.info(f"Editing todo: {todo}:{tag_choose}, {limit=}")
    todo.title = title
    todo.details = details
    todo.tag_choose = models.tag_from_str(tag_choose)
    todo.completed = completed
    database.commit()
    return RedirectResponse(url=get_home_url(skip, limit=limit), status_code=status.HTTP_303_SEE_OTHER)


@app.post("/edit_complete/{todo_id}")
async def todo_complete_edit(
        todo_id: int,
        limit: int = 10,
        skip: int = 0,
        database: Session = Depends(get_db)):
    """Edit todo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if todo is None:
        logger.warning("Editing not exist todo interrupted")
        return RedirectResponse(url=get_home_url(skip, limit=limit), status_code=status.HTTP_303_SEE_OTHER)
    logger.info(f"Editting todo: {todo}")
    todo.completed = not todo.completed
    todo.fulfill_datetime = datetime.now().date()
    database.commit()
    return RedirectResponse(url=get_home_url(skip, limit=limit), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/visualize")
async def visualize(request: Request, database: Session = Depends(get_db)):
    """Visualize page
    """
    active_user = database.query(models.UserInDB).filter(models.UserInDB.disabled == False).first()
    authorized = False
    if active_user:
        authorized = True
    sizes = []
    label = []
    todos_tag = (database.query(models.Todo.tag_choose, func.count(models.Todo.tag_choose))
                 .group_by(models.Todo.tag_choose)).all()

    for tag in todos_tag:
        sizes.append(tag[1])
        label.append(tag[0] + " ")
    squarify.plot(
        sizes=sizes,
        color=['#56567F', '#64CDCC', '#5FBB68', '#FD6F30'],
        label=label,
        alpha=0.7,
        pad=0.2,
        text_kwargs={'fontsize': 10, 'color': 'black'},

    )
    plt.axis('off')
    plt.savefig('static/pictures/fig.png')
    plt.close()

    return templates.TemplateResponse("visualize.html", {"request": request,
                                                         "authorized": authorized})


@app.get("/delete/{todo_id}")
async def todo_delete(todo_id: int, skip: int = 0, limit: int = 10, database: Session = Depends(get_db)):
    """Delete todo
    """
    logger.info(f"Deleting todo, {limit=}")
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if todo is None:
        logger.warning("Deleting not exist todo interrupted")
        return RedirectResponse(url=get_home_url(skip, limit=limit), status_code=status.HTTP_303_SEE_OTHER)
    logger.info(f"Deleting todo: {todo}, {get_home_url(skip, limit=limit)=}, {limit=}")
    database.delete(todo)
    database.commit()
    return RedirectResponse(url=get_home_url(skip, limit=limit), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/export")
def export_todo(database: Session = Depends(get_db)):
    """Export db.sqlite in excel
    """
    todos = database.query(models.Todo).all()
    id_list = []
    title_list = []
    details_list = []
    completed_list = []
    tag_choose_list = []
    current_datetime_list = []
    fulfill_datetime_list = []

    for todo in todos:
        id_list.append(todo.id)
        title_list.append(todo.title)
        details_list.append(todo.details)
        completed_list.append(todo.completed)
        tag_choose_list.append(todo.tag_choose)
        current_datetime_list.append(todo.current_datetime)
        fulfill_datetime_list.append(todo.fulfill_datetime)

    data_frame = pd.DataFrame({
        'id': id_list,
        'title': title_list,
        'details': details_list,
        'completed': completed_list,
        'tag_choose': tag_choose_list,
        'current_datetime': current_datetime_list,
        'fulfill_datetime': fulfill_datetime_list
    })

    data_frame.to_excel("./data/todos.xlsx")
    logger.info("excel file - done")
    return FileResponse("./data/todos.xlsx",
                        filename="todos.xlsx",
                        media_type="application/octet-stream"
                        )


@app.post("/import")
def import_todo(uploading_file: UploadFile, database: Session = Depends(get_db)):
    """Import excel to db.sqlite
    """
    path_file = uploading_file.file.read(-1)

    f = open(f'/code/app/data/{uploading_file.filename}', 'wb')
    f.write(path_file)
    f.close()

    todo = pd.read_excel(path_file)
    logger.info(f"import file: \n{todo}")

    dict_todo = todo.to_dict("records")
    ids = database.query(models.Todo.id).all()

    for line in dict_todo:
        id_field = line["id"]
        title_field = line["title"]
        details_field = line["details"] if isinstance(line["details"], str) else ""
        completed_field = line["completed"]
        tag_choose_field = line["tag_choose"]
        current_datetime_field = line["current_datetime"]
        fulfill_datetime_field = line["fulfill_datetime"]
        if (id_field,) in ids:
            logger.info(f"skip line with id : {id_field}")
            continue
        if not completed_field and fulfill_datetime_field != "":
            logger.error("validation failed, task not complited but fulfill_datetime not empty")
        todo = models.Todo(id=id_field, title=title_field, tag_choose=tag_choose_field,
                           details=details_field, completed=completed_field,
                           current_datetime=current_datetime_field,
                           fulfill_datetime=fulfill_datetime_field
                           )
        database.add(todo)
    database.commit()

    return RedirectResponse(url=get_home_url(), status_code=status.HTTP_303_SEE_OTHER)


@app.post("/generate")
async def generate(count: Union[str, None] = Form(None),
                   database: Session = Depends(get_db)):
    """Generate todos
       """
    count = str_to_int(count, 10)
    for i in range(count):
        title = randint(1, 20)
        todo = models.Todo(title=title, tag_choose=random.choice([models.Tag.study.name,
                                                                  models.Tag.personal.name,
                                                                  models.Tag.plans.name
                                                                  ]))
        database.add(todo)
        database.commit()
    return RedirectResponse(url=get_home_url(0), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/generate")
async def generate(request: Request, database: Session = Depends(get_db)):
    """Generate todos
       """
    active_user = database.query(models.UserInDB).filter(models.UserInDB.disabled == False).first()
    authorized = False
    if active_user:
        authorized = True
    return templates.TemplateResponse("generate.html", {"request": request,
                                                        "authorized": authorized})


@app.get("/import_issues")
async def import_issues_page(request: Request, database: Session = Depends(get_db)):
    """page with todo list
    """
    active_user = database.query(models.UserInDB).filter(models.UserInDB.disabled == False).first()
    authorized = False
    if active_user:
        authorized = True
    logger.info("In /import_issues")

    todos = database.query(models.Todo).order_by(models.Todo.id.desc())
    count = todos.count()

    return templates.TemplateResponse("import_issues.html", {"request": request,
                                                             "authorized": authorized})


def fromutcformat(utc_str, tz=None):
    iso_str = utc_str.replace('Z', '+00:00')
    return datetime.fromisoformat(iso_str).astimezone(tz)


@app.post("/import_issues")
async def import_issues(request: Request,
                        url: str | None = Form(None),
                        token: str | None = Form(None),
                        database: Session = Depends(get_db)
                        ):
    """page with todo list
    """
    logger.info(f"In /import_issues, {url=}, {token=}")

    headers = {
        'PRIVATE-TOKEN': token,
    }

    response = requests.get(url, headers=headers)
    logger.info(f"{type(response), response}")
    try:
        for i in response.json():
            title = i['title']
            description = i['description']
            created_at = i['created_at']

            if title is None:
                logger.warning("Creating empty todo interrupted")

            if len(title) > 500:
                logger.warning("Creating too long (<500 chars) todo interrupted")
                title = title[0:496] + "..."

            todo = models.Todo(title=title, details=description)
            todo.current_datetime = fromutcformat(created_at)  # datetime.fromtimestamp(created_at)
            logger.info(f"Creating todo: {todo}")
            database.add(todo)
            database.commit()

        return RedirectResponse(url=get_home_url(), status_code=status.HTTP_303_SEE_OTHER)
    except:
        return RedirectResponse(url=get_home_url(), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/datefiter")
async def date_filter(request: Request, date: str, database: Session = Depends(get_db)):
    check_date = date.split("-")[2] + "-" + date.split("-")[1] + "-" + date.split("-")[0]
    todos = database.query(models.Todo).filter(models.Todo.creation_date >= check_date)
    return templates.TemplateResponse("filter.html", {"request": request,
                                                      "todos": todos.all()})


@app.get("/import_log")
async def import_log_page(request: Request, database: Session = Depends(get_db)):
    """page with import log
    """
    active_user = database.query(models.UserInDB).filter(models.UserInDB.disabled == False).first()
    authorized = False
    if active_user:
        authorized = True

    files = []
    directory = pathlib.Path('./data/')
    pattern = "*.xlsx"
    for currentFile in directory.glob(pattern):
        files.append(currentFile.name)

    return templates.TemplateResponse("import_log.html", {"request": request,
                                                          "authorized": authorized,
                                                          "files": files})

'''
@app.get("/image/{todo_id}")
async def upload_img(request: Request, uploading_file: UploadFile, todo_id: int,  database: Session = Depends(get_db)):

    try:
        path_file = uploading_file.file.read(-1)
        todo = pd.read_excel(path_file)


        todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    except Exception as error:
        logger.info(error)'''

@app.get("/choose/{todo_id}")
async def choose_image(request: Request, todo_id: int, database: Session = Depends(get_db), image: str = None):
    """Choose from uploaded images"""
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if image != None:
        todo.image = image
        database.commit()
        return RedirectResponse(url=app.url_path_for(name="home"),
                                status_code=status.HTTP_303_SEE_OTHER)
    todos = database.query(models.Todo).filter(models.Todo.image != "")
    return templates.TemplateResponse("lib.html", {"request": request, "todo_by_id": todo, "todos": todos, "todo_id": todo_id})

@app.get("/favicon.ico")
async def todo_favicon():
    """Отображение фавикона
    """
    return FileResponse(f"static/favicon.ico")
