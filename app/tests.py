# pip install httpx
import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from fastapi import FastAPI, Request, Depends, Form, status, UploadFile

from app import models
from main import app
from database import init_db, get_db, Session
import pandas as pd
client = TestClient(app)


def test_delete_None():
    todo_id = None
    response = client.get(f"/delete/{todo_id}")
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_delete_todo():
    response = client.get(f"/delete/1")
    assert response.status_code == status.HTTP_200_OK



# @pytest.fixture
# def path():
#     base = pd.DataFrame.from_dict(columns=['id', 'title', 'tag_choose',
#                        'details', 'completed', 'current_datetime',
#                        'fulfill_datetime'])
#     todo = models.Todo(id="id_field", title="title_field", tag_choose="tag_choose_field",
#                        details="details_field", completed="completed_field",
#                        current_datetime="current_datetime_field",
#                        fulfill_datetime="fulfill_datetime_field"
#                        )
#
#
#
# def test_import_todo(database: Session = Depends(get_db)):
#     """Import excel to db.sqlite
#     """
#     with TestClient() as client:
#         response = client.get("/import")
#     todo = database.query(models.Todo.id).filter(models.Todo.id == "5").first()
#     assert response.status_code == 303
