import pytest
import app
from database import init_db, get_db, Session
import pandas as pd
import models
from fastapi.testclient import TestClient


@pytest.fixture
def test_exel():
    data = pd.DataFrame.from_dict({"id": 0, "title": "test",
                                   "details": "test_details", "completed": True,
                                   "tag": "personal", "current_datetime": "03-06-2020",
                                   "fulfill_datetime": "03-06-2022", "fullname": "2020-4-17-nem"})
    data.to_excel('test_data.xlsx', sheet_name="todos", index=False)
    return "test_data.xlsx"


def test_import(database=next(get_db())):
    with TestClient(app) as client:
        response = client.get(f"/import?file_name={test_exel}")
        todo = database.query(models.Todo).filter(models.Todo.id == '1000').first()
        assert response.status_code == 200
        assert todo.title == "test"
        assert todo.full == "2020-4-17-nem"
