"""Todo models
"""
from datetime import datetime
import enum
from pydantic import BaseModel
from sqlalchemy import Column, Integer, Boolean, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Tag(enum.Enum):
    default = "input:"
    study = "учёба"
    personal = "личное"
    plans = "планы"


class Todo(Base):
    """Todo model
    """
    __tablename__ = 'todos'
    id = Column(Integer, primary_key=True)
    title = Column(Text)
    details = Column(Text, default="")
    completed = Column(Boolean, default=False)
    tag_choose = Column(Text, default=Tag.default.name)
    current_datetime = Column(Text, default=datetime.now().date())
    fulfill_datetime = Column(Text, default="")
    fullname = Column(Text)
    picture = Column(Text, default="")  # Поле в бд для хранения пути к картинке, сама картрнка на диске.

    def __repr__(self):
        return f'<Todo {self.id}>'


def tag_from_str(string):
    ans = None
    for item in Tag:
        if string == item.name:
            ans = item.name
    return ans


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


class User(Base):
    __abstract__ = True
    id = Column(Integer, primary_key=True)
    username = Column(Text)
    disabled = Column(Boolean, default=True)


class UserInDB(User):
    __tablename__ = "logins"
    hashed_password = Column(Text)

    def __repr2__(self):
        return f'<User {self.id}>'
