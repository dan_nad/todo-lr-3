"""Database for todo
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session  # pylint: disable=unused-import

from models import Base

ENGINE = create_engine("postgresql://postgres:postgres@172.17.0.1:5432", echo=True)
SESSIONLOCAL = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)

def init_db():
    """Init database, create all models as tables
    """
    # check_same_thread is for SQLite only
    Base.metadata.create_all(bind=ENGINE)

def get_db():
    """Create session/connection for each request
    """
    database = SESSIONLOCAL()
    try:
        yield database
    finally:
        database.close()
